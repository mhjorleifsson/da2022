

### Welcome to Trek10 Developer Accelerator

The developer accelerator workshop series teaches experienced developers how to build modern applications and architecture patterns. Our AWS-certified experts will lead them through a variety of relevant topics, best practices, and hands-on labs using AWS. 

### Who needs this workshop?

- In-house development teams with company-wide goals to modernize applications and workloads
- Organizations with high workloads on AWS
- Teams with monolithic Java applications that need modernization
- Companies with motivation to develop cloud-native applications
- Leaders who want to level-up their developers