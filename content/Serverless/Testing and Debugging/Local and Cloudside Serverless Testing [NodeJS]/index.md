---
title: "Local and Cloudside Testing [NodeJS]"
date: 2022-10-21T17:24:42-04:00
draft: false
weight: 2
---

In this workshop, we’ll create a sample serverless application using AWS SAM.

We’ll test local changes to our function while using cloud permissions and services.

### Prerequisites

- AWS CLI already configured with Administrator permission

- AWS SAM CLI installed

- NPM installed

- Docker installed

### Steps

We’ll begin by initializing a sample SAM application.

1. Run the following command in your terminal from your projects directory: sam init -r nodejs14.x

2. Select 1 - AWS Quick Start Templates

3. Select 1 - Zip

4. Give it a custom name unique to you

5. Select 1 - Hello World Example

6. Open the created application directory in your code editor

7. Explore the files created. Note the function code under the hello-world folder and the SAM template defining our infrastructure in template.yaml

## Full Code Walkthrough

```
$ sam init -r nodejs14.x
Which template source would you like to use?
	1 - AWS Quick Start Templates
	2 - Custom Template Location
Choice: 1
What package type would you like to use?
	1 - Zip (artifact is a zip uploaded to S3)	
	2 - Image (artifact is an image uploaded to an ECR image repository)
Package type: 1

Project name [sam-app]: 

Cloning from https://github.com/aws/aws-sam-cli-app-templates

AWS quick start application templates:
	1 - Hello World Example
	2 - Step Functions Sample App (Stock Trader)
	3 - Quick Start: From Scratch
	4 - Quick Start: Scheduled Events
	5 - Quick Start: S3
	6 - Quick Start: SNS
	7 - Quick Start: SQS
	8 - Quick Start: Web Backend
Template selection: 1

    -----------------------
    Generating application:
    -----------------------
    Name: sam-app
    Runtime: nodejs14.x
    Dependency Manager: npm
    Application Template: hello-world
    Output Directory: .
    
    Next steps can be found in the README file at ./sam-app/README.md
        

SAM CLI update available (1.33.0); (1.29.0 installed)
To download: https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html
$ cd sam-app/
$ ls -al
total 32
drwxr-xr-x  7 staff  staff   224 Oct 20 09:54 .
drwxr-xr-x  3 staff  staff    96 Oct 20 09:54 ..
-rw-r--r--  1 staff  staff  2011 Oct 20 09:54 .gitignore
-rw-r--r--  1 staff  staff  8147 Oct 20 09:54 README.md
drwxr-xr-x  3 staff  staff    96 Oct 20 09:54 events
drwxr-xr-x  6 staff  staff   192 Oct 20 09:54 hello-world
-rw-r--r--  1 staff  staff  1625 Oct 20 09:54 template.yaml
```
