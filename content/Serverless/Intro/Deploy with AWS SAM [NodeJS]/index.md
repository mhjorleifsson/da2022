---
title: "First application with SAM [NodeJS]"
date: 2022-10-21T17:24:42-04:00
draft: false
---

### Prerequisites
- The AWS CLI is installed and has admin permissions
- AWS SAM CLI installed
- NodeJS (10+, preferably 12) installed
- Docker installed

### Overview
The AWS Serverless Application Model (SAM) is a configuration framework for defining and deploying serverless applications. It’s based on CloudFormation, so it’s compatible with non-serverless AWS components as well.

You can use SAM when you want to manage Lambda function code and tie it to other AWS services via events.

### Steps

We’ll begin by initializing a sample SAM application. We’ll use the NodeJS language.

1. Run the following command in your terminal to create a sample SAM application called `sam-app` in your current directory: `sam init -r nodejs12.x`

2. At the “Which template source would you like to use?” prompt, choose 1

3. Choose defaults for the other prompts

4. At the “AWS quick start application templates:” prompt, choose 1

5. At your terminal, type `cd sam-app` and explore the files created, or open the `sam-app` directory in your text editor (for VSCode: `code sam-app`)

* ***Note*** the function code under the hello_world folder and the SAM template defining our infrastructure in template.yaml. The README file is also quite helpful — take a few minutes to read it.*

### Deploying your application
This sample application creates an API with a single route – the `/hello` resource which returns a simple message when invoked. Let’s deploy it to the cloud and see what happens.

The SAM command you will probably use the most is `deploy`. `sam deploy` does a bunch of useful things:

- Uploads your Lambda code to S3 as a zipped artifact

- Places the S3 file location in a deployable version of your SAM template.

- Turns your SAM configuration into AWS CloudFormation resources and deploys them as a CloudFormation stack in your AWS environment.

SAM also can write a TOML config file to your project directory to speed up future deployments. Let’s see how this works by performing an initial deployment of our sample application.

1. Run the following command in your `sam-app` directory: `sam deploy --guided`

2. Select the default prompts (you may want to choose a different stack name than “sam-app” if you are sharing your test AWS account with other workshop participants)

3. In the console output, you will see a few different things happening as SAM writes your deployment preferences to the `samconfig.toml` file for future use, creates an S3 bucket to upload your code, creates a CloudFormation changeset, and deploys the stack.

4. When the stack finishes deploying, log into your AWS console and take a look at the deployed resources for your app under the “Applications” view of the Lambda service. You should see several resources deployed, including a Lambda function, an API Gateway, and an IAM role.

*You may be wondering where the IAM role came from, since it’s not explicitly defined in the SAM template. SAM creates a default role for your function with minimal permissions. Later on, when we interact with other AWS resources from our function, we’ll show how to add more permissions to our template.*
 
5. Test that your deployed application works — copy the “API endpoint” from the Application view and paste it into your browser. Add the “/hello” resource onto the end of the path and hit Enter. You should see {"message": "hello world"} displayed in the browser to indicate that your function is serving resources correctly.

*Keep this browser tab open — we’ll refer back to it throughout the workshop to check on our function.*

## Building with dependencies
Usually, applications are more complex than just a single self-contained Lambda handler. Assembling Lambda zip files can be a challenge. Fortunately, AWS SAM helps you package third-party libraries with your code. The sample application makes it easy to demonstrate this by including the “requests” library. Let’s see how it works.

1. Un-comment all the commented-out code in `hello_world/app.js` — the code that requires the “axios” package and uses it to retrieve the current IP address (don’t forget the *location* in the return).

2. Run `sam build` at the command line to package your dependencies in SAM’s project build artifact directory (by default, it’s the `.aws-sam/build` folder). 

***IMPORTANT NOTE:*** *Once you have run sam build the first time, you have to keep using it before future deploys, even if you have only changed your SAM template and not the Lambda code. This is because SAM caches your template in the build directory.*

3. Check the .aws-sam/build/HelloWorldFunction folder and notice the dependency folders that have been created to support the code.

4. Run sam deploy again. You don’t need the --guided option now, since your config preferences are saved in samconfig.toml.

5. Refresh your web browser to see an updated response from the function – it should now be returning the IP address as part of the message.

### Adding a resource to the template

SAM supports ***several resource types*** by default – Lambda functions, API gateways, and DynamoDB tables are the most commonly used.

1. Let’s add a database table to our SAM application. Copy the following code into the Resources section of the template:
```
  HelloWorldTable:
    Type: AWS::Serverless::SimpleTable
    Properties:
      PrimaryKey:
        Name: id
        Type: String.
```

2. Run your `sam build` and `sam deploy` commands again to update the cloud infrastructure. You should now see a DynamoDB table added to the *Applications* view in the Lambda console.

### Updating Lambda code to interact with an AWS service

1. Now let’s update our code to interact with the database. The first thing we need to do is make our Lambda function aware of our DynamoDB table. There are a few ways to do this, but for now we can use a Lambda function environment variable. Add the following configuration to the *Properties* of the *HelloWorldFunction* resource:

```
      Environment:
        Variables:
            TABLE_NAME: !Ref HelloWorldTable
```

This configuration uses CloudFormation’s built-in reference syntax to pass the DynamoDB table name as an environment variable to the function. Now add the following code to the top of hello_world/app.py:
```
// other requires

const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient({apiVersion: '2012-08-10'});

async function updateIp(ipObject){
    const ip = ipObject.data.trim();
    var params = {
        TableName: process.env['TABLE_NAME'],
        Item: {
            id: Date.now().toString(),
            ip: ip
        }
    };
    await docClient.put(params).promise();
}

// rest of code
```

This code pulls the DynamoDB table name from the environment variables, creates a DynamoDB connection, and saves the function IP address in the database table along with the current timestamp. You should also add a call to the updateIp function in the Lambda handler directly after we get the ret object.

```
    await updateIp(ret);
```

2.Finally we need to add DynamoDB access permissions to our Lambda function. Paste the following configuration into the Properties section of your Lambda configuration in template.yaml:

```
      Policies:
        - Version: '2012-10-17'
          Statement:
            - Effect: Allow
              Action: dynamodb:PutItem
              Resource: !GetAtt HelloWorldTable.Arn
```

This configuration gives us write permissions only on the DynamoDB specified in our template — least privilege is always a good design principle.

1. Run your SAM build and deploy commands to update the application in AWS.

2. Hit refresh in your browser to run the function again. You shuold see the same output as before.

3. Navigate to DynamoDB in the AWS console and check out the table created by your application. You should see an item created with the timestamp and IP of the function call.

### Adding a scheduled event trigger to our function
We’ve now built a serverless application that presents an API and stores state, keeping track of its own invocations and their IP addresses. Now let’s suppose we wanted to run this function on a schedule, perhaps every hour, to determine if the Lambda IP is changing.

SAM provides integrations with a ***number of AWS event sources***, including CloudWatch Event rules. Let’s see how we can add a scheduled invocation to our function.

1. Add the following configuration to the Events section of the function properties in template.yaml:
```
        CheckIPScheduledEvent:
          Type: Schedule
          Properties:
            Schedule: rate(1 minute)
```

2. Build and deploy with your trusty SAM commands. This should be old hat by now!

3. Wait a minute, then check the DynamoDB table again. You should see a new IP address record indicating the function was invoked by the rule.

4. We set the rule at a minute for quick testing – change it to an hour and redeploy.

### What did we learn?

Deep breath … that was a lot! We created our first serverless application with AWS SAM, including a Lambda function, an API gateway, a DynamoDB table, and a scheduled event. We learned how to package third-party dependencies and deploy code changes. Not bad for an intro to serverless! Now, stretch yourself with these challenge questions.

### Want a challenge?

1. Besides environment variables, another way to send the configuration to Lambda functions is via the SSM Parameter Store. Try publishing the DynamoDB table name to SSM in your SAM Template by defining an AWS::SSM::Parameter resource. Now can you update the Lambda function code to pull the table name from SSM using the boto3 SDK “GetParameter” call?

2. Try making the function fail by adding or removing some code. How does the API respond? Can you find the error message? Check out the “Monitoring” tab in the Applications view for the SAM app on the Lambda console for some interesting dashboards and links to logs.

3. How does the function behave if you increase the amount of memory? Why do you think it behaves this way?