+++
archetype = "chapter"
title = "Serverless"
weight = 4
+++

Serverless Architecture is a way to build and run applications without managing infrastructure.

Applications still runs on servers, but AWS manages them.

It helps you focus on application development, improvements and rapid deployments.

