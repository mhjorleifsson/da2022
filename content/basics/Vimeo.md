---
title: "Silence of the Lambdas [Vimeo Test]"
date: 2022-10-21T17:24:42-04:00
draft: true
---

<iframe src="https://player.vimeo.com/video/322943628?h=580abb2ed2" width="720" height="480" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/322943628">AWS Lambda Serverless 101 - All Things Open 2017</a> from <a href="https://vimeo.com/duckbillgroup">The Duckbill Group</a> on <a href="https://vimeo.com">Vimeo</a>.</p>