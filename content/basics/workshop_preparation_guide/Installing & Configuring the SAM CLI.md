---
title: "Install & Configure the SAM CLI"
date: 2022-10-21T17:24:42-04:00
draft: false
---

The SAM CLI installation requires homebrew for macOS and Linux, and Windows it is packaged as an MSI.

Follow the AWS provided instructions. 

***https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html*** 

***We do recommend following the Docker installation step as a few of our workshop topics will require its usage***.

***Note:***  *It may take a few minutes for this installation to complete, as homebrew is fairly dependent on your internet bandwidth and machine speed in general. Be patient!*

To confirm things are installed correctly run `sam --help` from your command line.