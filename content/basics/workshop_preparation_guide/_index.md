---
title: "Workshop Preparation Guide"
date: 2022-10-21T17:20:50-04:00
draft: false
---

So often, we focus so much on our content and preparing for the delivery that we overlook the necessary logistics of room setup and gathering of materials to ensure your environment is top notch for workshop delivery.

Utilize this guide each time you set up and prepare your workshop materials and space.
