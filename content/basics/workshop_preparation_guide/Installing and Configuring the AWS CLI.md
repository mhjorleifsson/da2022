---
title: "Install & Configure the AWS CLI"
date: 2022-10-21T17:23:34-04:00
draft: false
---

AWS provides documentation for the installation of the AWS CLI. You can find it at ***https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html***.

Note that we recommend installing v2 (not available through pip).

For macOS and Linux, we recommend the ***bundled installer*** which prevents issues with system python that may occur.

For Windows, take a look at the ***MSI installer***.

Once installed, configure the AWS CLI with your credentials and/or profile for the AWS Account you will be using for this workshop. If you are not sure about this, ask your manager.

To confirm everything is working run aws `sts get-caller-identity` and confirm that you see information about your user and that it references the account you expect in the returned information. You may need to include the `--profile` flag in your command.