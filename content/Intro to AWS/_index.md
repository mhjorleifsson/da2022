+++
archetype = "chapter"
title = "Intro to AWS"
weight = 2
+++

Amazon Web Services (AWS) is a comprehensive cloud computing platform that includes infrastructure as a service (IaaS) and platform as a service (PaaS) offerings. AWS services offer scalable solutions for compute, storage, databases, analytics, and more.

In this module we will familiarize your team with some of the basics to get you started
