---
title: "Big Ideas"
date: 2022-10-21T17:24:42-04:00
draft: false
---

## Big Ideas
AWS Organizations is a service that lets you create and manage separate AWS accounts. A few of the key terms are:

* ***Organization:*** A collection of AWS Accounts or Organizational Units (OU)

* ***Organizational Unit:*** Used to logically group accounts together

* ***Service Control Policy:*** A policy that can be used to deny access to specific IAM actions across an entire account or OU

* ***Master account:*** An AWS account that serves as the “owner” of the organization, it is the account that actually creates the organization

* ***Member account:*** An AWS account that lives within an organization

* ***AWS Organizations*** make the following the multi-account structure best practice much easier to follow. Member accounts can be created without needing to manage root access keys, payment details, or personal information.

