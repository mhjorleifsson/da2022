<h1 align="center">Trek10 Developer Accelerator 2022</h1>

<p align="center">

<img src="https://badges.frapsoft.com/os/v1/open-source.svg?v=103" >

<img src="https://img.shields.io/github/stars/silent-lad/Vue2BaremetricsCalendar.svg?style=flat">

<img src="https://img.shields.io/github/issues/silent-lad/Vue2BaremetricsCalendar.svg">

<img src="https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat">
</p>

Developer Accelerator Source Control Project

---

Goal: Revise DA content and provide a framework in which it can be customized and source controlled

Redesigned and Wrapped with Hugo

<p align="center">
<img src="https://media1.tenor.com/images/bcb6b843c7863584b0c1329ebe671949/tenor.gif?itemid=6077591">
</p>

# Installation

Hugo projects use submodules. After cloning the repo, `init` and `update` any submodules (including the theme):

```
git submodule init
# then
git submodule update
```

You can also do this from the main project clone with:

`git clone --recurse-submodules https://gitlab.com/mhjorleifsson/da2022.git`

Now, install Hugo:

[macOS](https://gohugo.io/getting-started/installing#macos)

```
brew install hugo
# or
port install hugo
```

[Windows](https://gohugo.io/getting-started/installing#windows)

## Usage

### Customization

How to customize and select modules

```js
// config.toml

```



# Current Content -

| Level       | Title                              | Modified By                      |
| ----------  | ---------------------------------- | -------------------------------- |
| `100`       | Workstation Setup Basics           | M.Hjorleifsson                   |
| `100`       | Intro to AWS                       | Someone                          |
